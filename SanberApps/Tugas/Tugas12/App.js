import React, { Component } from 'react';
import {View,Text, StyleSheet, Image, TouchableOpacity, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItems from './components/videoitem';
import data from './data.json';
export default class App extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <Image style={styles.images} source={require('./images/logo.png')}/>
                    
                    <View style={styles.rightNav}>
                       <TouchableOpacity>
                       <Icon name="search" size={30} 
                       style={styles.navItem}/>
                       </TouchableOpacity>
                        
                        <TouchableOpacity>
                        <Icon name="account-circle" size={30} 
                        style={styles.navItem}/>
                        </TouchableOpacity>
                        
                    </View>

                </View>
                <View style={styles.body}>
                    <FlatList 
                    data={data.items}
                    renderItem={(video)=><VideoItems video={video.item}/>}
                    keyExtractor={(item)=>item.id}
                    ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#e5e5e5'}}/>}
                    />
                </View>
                <View style={styles.tabBar}>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="home" size={30}/>
                        <Text style={styles.tabTitle}>Home</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="whatshot" size={30}/>
                        <Text style={styles.tabTitle}>Trending</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="subscriptions" size={30}/>
                        <Text style={styles.tabTitle}>Subscriptions</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="folder" size={30}/>
                        <Text style={styles.tabTitle}>Library</Text>
                    </TouchableOpacity>
                </View>
            </View>   
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    navBar:{
        height:55,
        backgroundColor:'white',
        elevation:3,
        paddingHorizontal:15,
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    images:{
        width:98,
        height:22
    },
    rightNav:{
        flexDirection:'row',
    },
    navItem:{
        marginLeft:25,
    },
    body:{
        flex:1,

    },
    tabItem:{
        alignItems:'center',
        justifyContent:'center'
    },
    tabBar:{
        backgroundColor: 'white',
        height:60,
        borderTopWidth:0.5,
        borderColor:'#E5E5E5',
        flexDirection: 'row',
        justifyContent:'space-around'
    },
    tabTitle:{
        fontSize:11,
        color:'#3c3c3c',
        paddingTop:3
    }

});