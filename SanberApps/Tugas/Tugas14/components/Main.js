import React, {Component} from 'react';
import { View, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet } from 'react-native';

import Note from './Note';

export default class Main extends Component {
  render(){
    return(
      <View style={styles.container}>
            
            <View style={styles.header}>
              <Text style={styles.headerText}>
                  -NOTER-
              </Text>
            </View>

            <ScrollView style={styles.scrollContainer}>
            </ScrollView>

            <View style={styles.footer}>
              <TextInput
              style={styles.TextInput}
              placeholder='>note'
              placeholderTextColor='white'
              underlineColorAndroid='transparent'
              >

              </TextInput>
            </View>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
      flex: 1,
  },
  header: {
      backgroundColor: '#E91E63',
      justifyContent: 'center',
      alignItems: 'center',
      borderBottomWidth: 10,
      borderBottomColor: '#ddd'
  },
  headerText: {
      color: 'white',
      fontSize: 18,
      padding: 26,
      height:50
  },
  
});

