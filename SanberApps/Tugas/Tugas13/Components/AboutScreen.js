import React, { Component } from 'react';
import {View,
        Text, 
        Image, 
        StyleSheet,
        ScrollView,
        Button,
        Alert,
        TouchableOpacity,
        TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icons from 'react-native-vector-icons/MaterialIcons';

export default class App extends Component{
    render(){
        return(
                <ScrollView>
                    <View style={styles.container}>

                        <View style={styles.loginArea}>
                            <Icon name="ios-arrow-round-back" 
                            size={50} 
                            color="#2F2E41" 
                            style={{alignItems:'flex-start'}}/>
                            <Text style={
                                styles.page
                            }>About Me</Text>
                    </View>

                
                        <View>
                            <Icons name="person" size={180} color="#10AC84"/>
                        </View>
                        
                        <View style={styles.name}>
                            <Text style={styles.aturkata}>Name : </Text>
                            <Text style={
                             styles.isi
                            }>Faizal Ardhi Cahyanto</Text>
                        </View>

                        <View style={styles.name}>
                            <Text style={styles.aturkata}>Email : </Text>
                            <Text style={
                                styles.isi
                            }>faizalardhican@gmail.com</Text>
                        </View>

                        <View style={styles.name}>
                            <Text style={styles.aturkata}>Place / Birthday : </Text>
                            <Text style={ styles.isi
                            }> Surakarta, 16 Maret 1997 </Text>
                        </View>
                       
                       <View style={styles.name}>
                            <Text style={styles.aturkata}> Media Social Account  : </Text>
                            <View style={{
                                backgroundColor:'#10AC84',
                                width:280,
                                marginTop:15,
                                alignItems:'center',
                                borderRadius:15
                            }}>
                            
                                    <View style={styles.medsos}>
                                        <Icon name="logo-facebook" size={50} color="#10AC84"/>
                                        <Text style={{
                                            fontSize:15,
                                            color:'#10AC84',
                                            padding:15
                                        }}>Faizal Ardhi Can</Text>
                                    </View>

                                    <View style={styles.medsos}>
                                        <Icon name="logo-twitter" size={50} color="#10AC84"/>
                                        <Text style={{
                                            fontSize:15,
                                            color:'#10AC84',
                                            padding:15
                                        }}>faizalardhi16</Text>
                                    </View>

                                    <View style={styles.medsos}>
                                        <Icon name="logo-instagram" size={50} color="#10AC84"/>
                                        <Text style={{
                                            fontSize:15,
                                            color:'#10AC84',
                                            padding:15
                                        }}>faizal.ac16</Text>
                                    </View>

                                    <View style={styles.medsos}>
                                        <Icon name="logo-youtube" size={50} color="#10AC84"/>
                                        <Text style={{
                                            fontSize:15,
                                            color:'#10AC84',
                                            padding:15
                                        }}>Faizal Ardhi</Text>
                                    </View>

                            </View>
                       </View>
                    
                        
                         
                            
                    </View>
                </ScrollView>
        
        );
    }
}


const styles = StyleSheet.create({
    
    container:{
        justifyContent:'center',
        alignItems:'center',
        margin:15,
        flexDirection:'column'
    },
    logo:{
        flexDirection:'row',
    },
    loginArea:{
        height:50,
        width:360,
        backgroundColor:'#10AC84',
        paddingLeft:20,
        flexDirection:'row',
    },
    page:{
            alignItems:'center',
            paddingLeft:80,
            paddingRight:80,
            paddingTop:10,
            paddingBottom:10,
            fontSize:20,
            color: '#2F2E41'
        },
    under:{
        height:50,
        width:360,
        alignItems:'stretch',
        backgroundColor:'#10AC84',
        paddingLeft:20,
        margin:20
    },
    aturkata: {
        color:'#10AC84',
        fontSize:20,
        textAlign:'left',
        marginTop:10
    },
    name:{
        alignItems:'flex-start',
        justifyContent:'flex-start',
        width:280,
        flexDirection:'column'
    },
    isi:{
        marginTop:10, 
        backgroundColor: '#10AC84',
        padding:20,
        width:280,
        fontSize:20,
        borderRadius:15
    },
    medsos:{
        backgroundColor:'#2F2E41',
        width:200,
        margin:10,
        padding:20,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:15,
        flexDirection:'row'
    }
    
})

