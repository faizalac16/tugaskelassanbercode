import React, { Component } from 'react';
import {View,
        Text, 
        Image, 
        StyleSheet,
        ScrollView,
        Button,
        Alert,
        TouchableOpacity,
        TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default class App extends Component{
    render(){
        return(
         
                    <View style={styles.container}>

                        <View style={styles.loginArea}>
                            <Icon name="ios-arrow-round-back" 
                            size={50} 
                            color="#2F2E41" 
                            style={{alignItems:'flex-start'}}/>
                            <Text style={
                                styles.page
                            }>Register Area</Text>
                        </View>
                        
                       
                            <Image 
                            source={require('../Images/vrr.png')}
                            style={{
                                width:270,
                                height:270,
                                alignItems:'center'                        
                            }}
                            />
                        
                        <View style={{
                            flexDirection:'row',
                            backgroundColor: '#636175',
                            width:300,
                            height:60,
                            padding:10,
                            paddingLeft:20,
                            margin:10,
                            borderRadius:15
                        }}>
                            <Icon name="ios-mail-open" size={38} color="#9290AC"/>
                            
                            <TextInput
                            placeholder="Email"
                            backgroundColor="transparent"
                            style={{
                                paddingBottom:0,
                                fontSize:30,
                                paddingLeft:20,
                                color:'#9290AC'  
                            }}></TextInput>
                        </View>

                        <View style={{
                            flexDirection:'row',
                            backgroundColor: '#636175',
                            width:300,
                            height:60,
                            padding:10,
                            paddingLeft:20,
                            margin:10,
                            borderRadius:15
                        }}>
                            <Icon name="ios-lock" size={38} color="#9290AC"/>
                            
                            <TextInput
                            placeholder="Password"
                            backgroundColor="transparent"
                            style={{
                                paddingBottom:0,
                                fontSize:30,
                                paddingLeft:20,
                                color:'#9290AC'  
                            }}></TextInput>
                        </View>

                        
                        <View style={{
                            flexDirection:'row',
                            backgroundColor: '#636175',
                            width:300,
                            height:60,
                            padding:10,
                            paddingLeft:20,
                            margin:10,
                            borderRadius:15
                        }}>
                            <Icon name="ios-lock" size={38} color="#9290AC"/>
                            
                            <TextInput
                            placeholder="Re-Password"
                            backgroundColor="transparent"
                            style={{
                                paddingBottom:0,
                                fontSize:30,
                                paddingLeft:20,
                                color:'#9290AC'  
                            }}></TextInput>
                        </View>

                        <View style={{flexDirection:'row', margin:1}}>
                            <Icon name="ios-square" size={38} color="#9290AC"/>   
                            <Text style={{color:"#9290AC",margin:10}}>I Agree With Terms And Condition</Text>
                        </View>
                          

                        <TouchableOpacity>
                            <View style={styles.under}>
                                    <Text style={{
                                        textAlign:'center',
                                        fontSize:30,
                                        paddingTop:5,
                                        alignItems:'stretch',
                                        justifyContent:'center'
                                    }}>SIGN UP</Text>
                            </View>
                        </TouchableOpacity>
                        
                         
                            
                    </View>
        
        );
    }
}


const styles = StyleSheet.create({
    
    container:{
        justifyContent:'center',
        alignItems:'center',
        margin:15,
        flexDirection:'column'
    },
    logo:{
        flexDirection:'row',
    },
    loginArea:{
        height:50,
        width:360,
        backgroundColor:'#10AC84',
        paddingLeft:20,
        flexDirection:'row',
    },
    page:{
            alignItems:'center',
            paddingLeft:80,
            paddingRight:80,
            paddingTop:10,
            paddingBottom:10,
            fontSize:20,
            color: '#2F2E41'
        },
    fontInput:{
        fontSize:30,
        paddingLeft:20,
        color:'#9290AC'
    },
    under:{
        height:50,
        width:360,
        alignItems:'stretch',
        backgroundColor:'#10AC84',
        paddingLeft:20,
        margin:20
    }
    
})

