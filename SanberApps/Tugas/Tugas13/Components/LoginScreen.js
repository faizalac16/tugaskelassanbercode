import React, { Component } from 'react';
import {View,
        Text, 
        Image, 
        StyleSheet,
        ScrollView,
        Button,
        Alert,
        TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default class App extends Component{
    render(){
        return(
                <ScrollView>
                    <View style={styles.container}>
                        <Text style={styles.judul}>Programae.IO</Text>

                        <Image source={require('../Images/code.png')} 
                        style={{width:150, height:150,padding:10, alignItems:'center', margin:20}}/>

                        <TouchableOpacity>
                            <View style={styles.border}>
                            <Text style={styles.TextLogin}>Login</Text>
                            </View>      
                        </TouchableOpacity>

                        <TouchableOpacity>
                            <View style={styles.border}>
                            <Text style={styles.TextRegister}>Register</Text>
                            </View>      
                        </TouchableOpacity>

                        <Text style={styles.with}>Or Login With : </Text>

                        <View style={styles.logo}>
                            <Icon name="logo-facebook" size={60} color="#10AC84" style={{padding:20}}/>
                            <Icon name="logo-instagram" size={60} color="#10AC84" style={{padding:20}}/>
                            <Icon name="ios-mail" size={60} color="#10AC84" style={{padding:20}}/>
                        </View>

                        
                         
                            
                    </View>
                </ScrollView>
        );
    }
}


const styles = StyleSheet.create({
    judul:{
        fontSize:30,
        color: '#10AC84',
        fontFamily:'Sans',
        fontWeight:"bold",
        margin:20
    },
    container:{
        justifyContent:'center',
        alignItems:'center',
        margin:15,
        flexDirection:'column'
    },
    TextLogin:{
        fontSize:30,
        color:'#10AC84',
        textAlign:'center',
        fontFamily:"Cochin"
    },
    TextRegister:{
        fontSize:30,
        textAlign:'center',
        color:'#10AC84',
        fontFamily:"Cochin"
    },
    border:{
        margin:20,
        width:280,
        height:80,   
        borderColor:'#10AC84',
        borderStyle:'solid',
        borderWidth:3,
        padding:15
    },
    with:{
        color: '#10AC84',
        fontSize:18,
        margin:20,
    },
    logo:{
        flexDirection:'row',
    },
    loginArea:{
        height:50,
        width:360,
        backgroundColor:'#10AC84',
        paddingLeft:20,
        flexDirection:'row',
    },
    page:{
            alignItems:'center',
            paddingLeft:80,
            paddingRight:80,
            paddingTop:10,
            paddingBottom:10,
            fontSize:20,
            color: '#2F2E41'
        }
    
})

