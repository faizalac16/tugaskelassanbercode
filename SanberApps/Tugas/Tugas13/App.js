import React, { Component } from 'react';
import {View,
        Text, 
        Image, 
        StyleSheet,
        ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Login from './Components/LoginScreen';
import LoginA from './Components/AboutScreen';


export default class App extends Component{
    render(){
        return(
            <>
            <View style={styles.container}>
                <LoginA/>
            </View> 
            </>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor:'#2F2E41'
    },
});
