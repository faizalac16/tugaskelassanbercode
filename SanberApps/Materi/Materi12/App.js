import React, { Component } from 'react';
import{
    View,
    Text,
    StyleSheet,
    ScrollView,
    Image,
    FlatList,
    TouchableOpacity,
    TextInput} 
from 'react-native';

export default class App extends Component{
    constructor(){
        super();
        this.state = {
          count : 0,
          value: '',
        }
      }

      onPlus=()=>{
          if(this.state.count<100) this.setState({ count: this.state.count + 1 }); 
      }

      onMins=()=>{
          if(this.state.count>0) this.setState({count:this.state.count - 1});  
      }

    render=()=>{
        return(
        <ScrollView>
            <View style={styles.container}>
                
         
                <View style={styles.flexbox}/>
                <View style={styles.flexbox1}/>
                <Text style={styles.text}>Hello World!</Text>
                <Image 
                style={styles.image} 
                source={{uri:'https://image.freepik.com/free-vector/designer-s-office-flat-illustration_23-2147492101.jpg'}}
                />

                <ScrollView horizontal>
                            <Image 
                                style={styles.image1} 
                                source={{uri:'https://image.freepik.com/free-vector/designer-s-office-flat-illustration_23-2147492101.jpg'}}
                            />
                            <Image 
                                style={styles.image1} 
                                source={{uri:'https://image.freepik.com/free-vector/designer-s-office-flat-illustration_23-2147492101.jpg'}}
                            />
                            <Image 
                                style={styles.image1} 
                                source={{uri:'https://image.freepik.com/free-vector/designer-s-office-flat-illustration_23-2147492101.jpg'}}
                            />
                            </ScrollView>

                            <View style={styles.countContainer}>
                                <Text>Count: {this.state.count}</Text>
                            </View>

                            <TouchableOpacity
                                style={styles.button}
                                onPress={() => this.onPlus()}
                            >
                                <Text>Tambahkan</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={styles.button}
                                onPress={() => this.onMins()}
                            >

                                <Text>Kurangi</Text>
                            </TouchableOpacity>

                            <TextInput
                            style={{ height: 40, borderColor: 'gray', borderWidth: 1 , marginTop:10}}
                            onChangeText={(text) => this.setState({ value: text })}
                            value={this.state.value}
                            placeholder={"Tulis Disini"}
                            />
                            <Text>{this.state.value}</Text>






            </View>

            
   
        </ScrollView>           
        );
    }

}

const Space = ()=>{
    return <Text style={{width:5}}> </Text>
};

const styles = StyleSheet.create({
        container: {
            flex:1,
            justifyContent:'center',
            alignItems:'center',
            backgroundColor:'skyblue'
        },
        flexbox: {
            width: 360,
            height: 100,
            borderWidth: 3,
            borderRadius:0,
            backgroundColor:'#9c88ff',
            borderColor: '#8c7ae6',
            marginTop:10
        },
        flexbox1: {
            width: 360,
            height: 100,
            borderWidth: 3,
            borderRadius:0,
            backgroundColor:'#e84118',
            borderColor: '#c23616',
            marginTop:10
        },
        text: {
            backgroundColor: '#1B1464',
            color: '#10ac84',
            borderColor: '#10ac84',
            borderWidth:3,
            fontSize: 24,
            padding: 10,
            marginTop:10,
            width:360,
            textAlign: 'center'
          },
          image:{
              width:200,
              height:200,
              marginTop:10
          },
          image1: {
            width: 200,
            height: 200,
            marginBottom: 10,
            marginRight: 10,
            marginTop:10,
            borderRadius:15,
          },
          row: {
            padding: 15,
            marginBottom: 5,
            backgroundColor: 'skyblue',
          },
          button: {
            alignItems: "center",
            backgroundColor: "#DDDDDD",
            padding: 10
          },
          countContainer: {
            alignItems: "center",
            padding: 10
          }
});
