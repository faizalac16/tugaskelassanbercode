console.log("\n");
console.log("Soal Nomor 1\n");

//looping pertama
console.log("looping pertama");
var loop1 = 2;

while(loop1 < 21 ){
    console.log(loop1 + ' - ' + ' I love coding');
    loop1 +=2;
}

console.log("\n");

//looping kedua
console.log("looping kedua");
var loop2 = 22;
var deret = 0;

while(loop2 > 2 ){
    deret +=loop2;
    loop2-=2;
    console.log(loop2 + ' - ' + ' I will become a mobile developer');
}

// Soal Nomor 2

console.log("\n");
console.log("Soal Nomor 2 \n");


for(var angka=1;angka<21;angka++){
    if(angka%2==1){
        if(angka%3==0){
            console.log(angka + " I Love Coding");
        }else{
            console.log(angka + " Santai");
        }
    }else{
        console.log(angka + " Berkualitas");
    }
}

//soal nomor 3
console.log("\n");
console.log("Soal Nomor 3 \n");


for(var t1 = 1; t1<5; t1++){
    var s = '';
    for(var t2=1;t2<9;t2++){
        s += "#";
    }
    console.log(s);
}


//soal no 4
console.log("\n");
console.log("Soal Nomor 4 \n");

for(var t1 = 1; t1<8; t1++){
    var j = '';
    for(var t2=1;t2<=t1;t2++){
        j += "#";
    }
    console.log(j);
}

//soal no 5
console.log("\n");
console.log("Soal Nomor 5 \n");
var papan =1;
for(var c1 = 1;c1<9;c1++){
    var cat = '';
      if(papan%2==1){
        for(var c2 = 1;c2<5;c2++){
            cat+= "#";
            cat+= " ";
        }
      }else{
        for(var c2 = 1;c2<5;c2++){
            cat += " ";
            cat+= "#";       
        }
      }
    papan+=1;
    console.log(cat);
}



