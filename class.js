console.log("------Soal No 1-------");
console.log("\n");


console.log("------ Release 0 : -------");
console.log("\n");
class Animal{
    constructor(name){
        this.name = name;
        this.legs = 4;
        this.cool_blooded = false;
    }

}

var sheep = new Animal("shaun");

console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cool_blooded);

console.log("\n");
console.log("------ Release 1 : -------");
console.log("\n");

class Ape extends Animal{
    constructor(name){
        super(name);
        this.legs = 2;
    }
    yell(){
        console.log("Auooo");
    }
}

var sungokong = new Ape("Kera Sakti");
sungokong.yell();

class Frog extends Animal{
    constructor(name){
        super(name);
    }
    jump(){
        console.log("hop hop");
    }
}

var kodok = new Frog("buduk");
kodok.jump();

console.log("\n");
console.log("------ 2 Function to Class : -------");
console.log("\n");

var secs;
var hours;
var mins;
class Clock{
    
    constructor({template}){

        this.template = template;
    }

    render(){
        this.date   = new Date();
        this.hours  = this.date.getHours();
        this.mins   = this.date.getMinutes();
        this.secs   = this.date.getSeconds();
        this.output = this.template.replace('h', this.hours).replace('m', this.mins).replace('s', this.secs);

        if (this.hours < 10) hours = '0' + this.hours;
        if (this.mins < 10) mins = '0' + this.mins;
        if (this.secs < 10) secs = '0' + this.secs;

        console.log(this.output);
    }

    stop(){
        this.stop = clearInterval(this.timer);
    }

    start(){
        this.render();
        this.timer = setInterval(()=>this.render(), 1000);

    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  