

console.log("\n\n");
console.log("Soal Nomor 1 \n ");

//soal 1

var word = 'JavaScript '; 
var second = 'is '; 
var third = 'awesome '; 
var fourth = 'and '; 
var fifth = 'I '; 
var sixth = 'love '; 
var seventh = 'it! ';

console.log(word.concat(second+third+fourth+fifth+sixth+seventh));

//Soal No.2 Mengurai kalimat (Akses karakter dalam string)

console.log("\n\n");
console.log("Soal Nomor 2 \n");


var sentence = "I am going to be React Native Developer"; 

var firstWord   = sentence[0] ; 
var secondWord  = sentence[2] + sentence[3];
var thirdWord   = sentence.substr(5,5); 
var fourthWord  = sentence.substr(11,2);  
var fifthWord   = sentence.substr(14,2); 
var sixthWord   = sentence.substr(17,5);  
var seventhWord = sentence.substr(23,6); 
var eighthWord  = sentence.substr(30,9); 

console.log('First Word: ' + firstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord);

//Soal No. 3 Mengurai Kalimat (Substring)

console.log("\n\n");
console.log("Soal Nomor 3 \n");

var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4,14); // do your own! 
var thirdWord2 = sentence2.substring(15,17); // do your own! 
var fourthWord2 = sentence2.substring(18,20); // do your own! 
var fifthWord2 = sentence2.substring(21,25); // do your own! 

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

// Soal Nomor 4

console.log("\n\n");
console.log("Soal Nomor 4 \n");

var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3   = sentence2.substring(0, 3); 
var secondWord3         = sentence3.substring(4,14); // do your own! 
var thirdWord3          = sentence3.substring(15,17); // do your own! 
var fourthWord3         = sentence3.substring(18,20); // do your own! 
var fifthWord3          = sentence3.substring(21,25); // do your own! 

var firstWordLength     = exampleFirstWord3.length; 
var secondWordLength    = secondWord3.length; 
var thirdWordLength     = thirdWord3.length; 
var fourthWordLength    = fourthWord3.length; 
var fifthWordLength     = fifthWord3.length; 
// lanjutkan buat variable lagi di bawah ini 
console.log('First Word: '  + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: '  + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: '  + fifthWord3 + ', with length: ' + fifthWordLength); 


//if else condition
console.log("\n\n");
console.log("Soal if Else condition \n");

var nama    = "Junaedi";
var peran   = "Werewolf";

if(nama == "" && peran ==""){
    console.log("Nama Harus Diisi");
}else if(nama=="John" && peran ==""){
    console.log("Silahkan Pilih Peran Anda ");
}else if(nama=="Jane" && peran == "Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, Jane");
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!");
}else if(nama=="Jenita" && peran =="Guard"){
    console.log("Selamat datang di Dunia Werewolf, Jenita");
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.");
}else if(nama=="Junaedi" && peran=="Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, Junaedi");
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!"); 
}else{
    console.log("Coba Masukkan Lagi, Pilihan Tidak Ada");
}

console.log("\n\n");
console.log("Tugas Switch And Case \n");

var tanggal = 29;
var bulan = 2;
var tahun = 1945;

if(tanggal < 1 || tanggal >31){
    console.log("Tanggal Salah Pilih");
}else if(bulan == 2 && tanggal <1 || tanggal >29){
    console.log("Februari Maksimal 29 hari");
}else if(bulan <1 || bulan>12){
    console.log("Bulan Salah Pilih");
}else if(tahun<1900 || tahun>2200){
    console.log("Tahun Tidak Termasuk dalam Daftar");
}else{
    switch(bulan){
        case 1:{ console.log( tanggal + " Januari " + tahun);break;}
        case 2:{ console.log( tanggal + " Februari " + tahun);break;}
        case 3:{ console.log( tanggal + " Maret " + tahun);break;}
        case 4:{ console.log( tanggal + " April " + tahun);break;}
        case 5:{ console.log( tanggal + " Mei " + tahun);break;}
        case 6:{ console.log( tanggal + " Juni " + tahun);break;}
        case 7:{ console.log( tanggal + " Juli " + tahun);break;}
        case 8:{ console.log( tanggal + " Agustus " + tahun);break;}
        case 9:{ console.log( tanggal + " September " + tahun);break;}
        case 10:{ console.log( tanggal + " Oktober " + tahun);break;}
        case 11:{ console.log( tanggal + " November " + tahun);break;}
        case 12:{ console.log( tanggal + " Desember " + tahun);break;}  
    }    
}








