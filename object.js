
console.log("\n");
console.log("---- Soal Nomor 1 Array To Object ---- \n");

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"],
              ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]];

function arrayToObject(arr){
    var c = arr.length;
    for(var i=0;i<c;i++){
        var tahun = arr[i][3];
        var age;
        if(tahun>=0 && tahun<=2020){
          age = 2020-tahun;
        }else{
          age = "Birthday Tidak Valid";
        }

        var biodata = {
          firstname: arr[i][0],
          secondname: arr[i][1],
          gender: arr[i][2],
          age: age
        }

        console.log(arr[i][0] + ' ' + arr[i][1] + ' : ' , biodata)
    }

}

var arr = arrayToObject(people);

console.log("\n");
console.log("---- Soal Nomor 2 Shopping Time ---- \n");

var money;
var memberId;

function shoppingTime(memberId,money){
  var belanjaan=[];
  var sisa = money;
  if(memberId&&sisa>=50000){

        if(sisa>=1500000){
          belanjaan.push("Sepatu Stacattu");
          sisa-=1500000;
        }
        if(sisa>=500000){
          belanjaan.push(" Baju Zoro ");
          sisa-=500000;
        }
        if(sisa>=250000){
          belanjaan.push(" Baju H&N ");
          sisa-=250000;
        }
        if(sisa>=175000){
          belanjaan.push(" Sweater Uniklooh ");
          sisa-=175000;
        }
        if(sisa>=50000){
          belanjaan.push(" Casing Handphone ");
          sisa-=50000;

    }

      var shopping = {
        memberId:memberId,
        money:money,
        listShopping: belanjaan,
        sisa: sisa
      }
  return shopping;
  }else if(!memberId&&money>=50000){
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  }else if(memberId&&sisa<50000){
    return "Mohon maaf, uang tidak cukup";
  }else{
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  }

}


console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("\n");
console.log("---- Soal Nomor 3 Naik Angkot ---- \n");

function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var c = arrPenumpang.length;
    if(!arrPenumpang){
      return "Dasar Biji";
    }else{
      var pejalan =[];
      for(var i=0;i<c;i++){
        var berangkat = rute.indexOf(arrPenumpang[i][1]);
        var tujuan = rute.indexOf(arrPenumpang[i][2]);
        var ongkos = tujuan-berangkat;
        ongkos = ongkos*2000;
        var penumpangAngkot = {
          penumpang: arrPenumpang[i][0],
          naikDari: arrPenumpang[i][1],
          tujuan: arrPenumpang[i][2],
          bayar: ongkos
        }

        pejalan.push(penumpangAngkot);
      }


    }
      return pejalan;


}


//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]
