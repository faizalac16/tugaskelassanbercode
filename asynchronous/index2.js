var readBooksPromise = require('./promise.js')

var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

// Lanjutkan code untuk menjalankan function readBooksPromise
var waktu = 10000;
function timeDown(x){
    var j=0;
    for(var i=waktu;i>0;i+=0){
      if(j==books.length){
        return 0;
      }
      readBooksPromise(i,books[j])
      .then(function(fulfilled){
        return fulfilled;
      })
      .catch(function(rejected){
        return rejected;
      })
      i-=books[j].timeSpent;
      j+=1;
    }

}

timeDown(0);
