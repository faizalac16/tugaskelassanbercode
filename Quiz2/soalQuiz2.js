/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  // Code disini
  
  constructor(ArrData){
    this.ArrData = ArrData;
  }
  score=(ArrData)=>{
    var average = [];
    for(var i=0;i<ArrData.length;i++){
      var newObj = {
        subject: ArrData[i][0],
        points: ArrData[i][1],
        email: ArrData[i][2]
      }
      console.log(newObj);
      average.push(ArrData[i][1]);
    }
    average = average.reduce((a, b) => a + b, 0);
    average = average/ArrData.length;
    console.log("Rata-Rata Nilai : " + average);
  }  
    
}

var ArrData = [['quiz MTK', 80, 'AbdulSamsul@gmai.com'],
              ['quiz Bio', 90, 'AbdulSamsul@gmai.com'],
              ['quiz Kimia', 80, 'AbdulSamsul@gmai.com']];

var score = new Score(ArrData);

score.score(ArrData);




/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

var email = data[0][0];
var quiz = data[0][1];
var quiz2 = data[0][2];
var quiz3 = data[0][3];

var viewScores = (data, subject) =>{
  // code kamu di sini
  if(subject=="quiz-1"){
    var i = 1;
    for(i;i<data.length;i++){
      var newObj = {
        email: data[i][0],
        subject:subject,
        points:data[i][1]
      }
      console.log(newObj);
    }
  }

  if(subject=="quiz-2"){
    var i = 1;
    for(i;i<data.length;i++){
      var newObj = {
        email: data[i][0],
        subject:subject,
        points:data[i][2]
      }
      console.log(newObj);
    }
  }

  if(subject=="quiz-3"){
    var i = 1;
    for(i;i<data.length;i++){
      var newObj = {
        email: data[i][0],
        subject:subject,
        points:data[i][3]
      }
      console.log(newObj);
    }
  }

    
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

function recapScores(data) {
  // code kamu di sini
  var i = 1;
  
  for(var i;i<data.length;i++){
    
    var nilai = (data[i][1]+data[i][2]+data[i][3])/3;

    if(nilai>70&&nilai<80){
      var newObj = {
        email: data[i][0],
        RataRata: nilai,
        Predikat: "Participant"
      }
    }
    
    if(nilai>80&&nilai<90){
      var newObj = {
        email: data[i][0],
        RataRata: nilai,
        Predikat: "Graduate"
      }
    }
    
    if(nilai>90){
      var newObj = {
        email: data[i][0],
        RataRata: nilai,
        Predikat: "Honour"
      }
    }

    console.log(newObj);
  }

}

recapScores(data);
