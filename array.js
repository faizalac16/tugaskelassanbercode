//soal nomor 1
console.log("Soal Nomor 1\n");

var startNum;
var finishNum;


function range(startNum,finishNum){
    var nilaiInput = [];

    if(startNum,finishNum){
        
        if(startNum<finishNum){
            for(var i = startNum; i<=finishNum;i++){
                nilaiInput.push(i);     
            }
            return nilaiInput;
        }else{
            for(var i = startNum; i>=finishNum;i--){
                nilaiInput.push(i);
            }
            return nilaiInput;
        }
            
    }else if(!startNum||!finishNum){
        return -1;
    }
        
}

console.log(range(1,10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54,50));
console.log(range());

//soal nomor 2
console.log("\n");
console.log("Soal No. 2 (Range with Step)\n");


var startNum1;
var finishNum1;
var step;

function rangeWithStep(startNum1,finishNum1,step){
    var nilaiInputs = [];
     if(startNum1,finishNum1,step){
        
        if(startNum1<finishNum1){
            for(var i = startNum1;i<=finishNum1;i++){
                nilaiInputs.push(i);
                i=i+step-1;     
            }
            return nilaiInputs;
        }else{
            for(var i = startNum1;i>=finishNum1;i--){
                nilaiInputs.push(i);  
                i=i-step+1;      
            }

            return nilaiInputs;
        }
    }
        
}

console.log(rangeWithStep(1,10,2));
console.log(rangeWithStep(11,23,3));
console.log(rangeWithStep(5,2,1));
console.log(rangeWithStep(29, 2, 4));

//soal nomor 3
console.log("\n");
console.log("Soal No. 3 (Sum of Range)\n");

function sum(startNum1,finishNum1,step){
    var nilaiInputs = [];
    

    if(startNum1,finishNum1,step){
        
        if(startNum1<finishNum1){
            for(var i = startNum1;i<=finishNum1;i++){
                nilaiInputs.push(i);
                i=i+step-1;     
            }
            return nilaiInputs.reduce((a, b) => a + b, 0)
        }else{
            for(var i = startNum1;i>=finishNum1;i--){
                nilaiInputs.push(i);  
                i=i-step+1;      
            }

            return nilaiInputs.reduce((a, b) => a + b, 0)
        }
    }else if(startNum1&&finishNum1&&!step){
        step =1;
        if(startNum1<finishNum1){
            for(var i = startNum1;i<=finishNum1;i++){
                nilaiInputs.push(i);
                i=i+step-1;     
            }
            return nilaiInputs.reduce((a, b) => a + b, 0)
        }else{
            for(var i = startNum1;i>=finishNum1;i--){
                nilaiInputs.push(i);  
                i=i-step+1;      
            }

            return nilaiInputs.reduce((a, b) => a + b, 0)
        }
    }else if(startNum1&&!finishNum1&&!step){
        return startNum1;
    }else{
        return 0;
    }
        
}

console.log(sum(1,10)); //55
console.log(sum(5,50,2)); //621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

//soal nomor 4
console.log("\n");
console.log("Soal No. 4 (Array Multidimensi)\n");

var i;

function dataHandling(i){
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ] 

    var j=0;
    
    return "ID : " + input[i][j] 
            + "\nNama : " + input[i][j+1] 
            + "\nTTL : " + input[i][j+2] + " " + input[i][j+3] + "\n" 
            + "Hobi : " + input[i][j+4] + "\n";
    
}

console.log(dataHandling(0));
console.log(dataHandling(1));
console.log(dataHandling(2));
console.log(dataHandling(3));

//soal nomor 5
console.log("\n");
console.log("Soal No. 5 (Balik Kata)\n");

var string;
var c;

function balikKata(string){
    
    var stringArr ="";
    
    c = string.length-1;

    for(var l=c;l>=0;l--){
        stringArr+= string[l];
    }

    return stringArr;
}

console.log(balikKata("Kasur RusaK"));
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 



